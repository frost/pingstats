## pingstats

This is a simple script to display a histogram of your ping times.

Usage:

```
ping wherever | pingstats
```

It parses ping's output, so things like `ping -f` won't work.
